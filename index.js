function calculateGrade() {
    const score = document.getElementById("score").value;
    

    if (score >= 90) {
        grade = "A";
    } else if (score >= 80) {
        grade = "B";
    } else if (score >= 70) {
        grade = "C";
    } else if (score >= 60) {
        grade = "D";
    } else if (score >= 50) {
        grade = "E";
    } else {
        grade = "F";
    }

    var grade= document.getElementById("result").innerHTML = "Your grade is: <span class='color'>" + grade + "</span>";
    document.getElementById("score").value = "";
}